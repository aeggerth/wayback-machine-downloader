# Use the official Ruby image from Docker Hub
FROM ruby:3.2

# Set the working directory inside the container
WORKDIR /usr/src/app

COPY . .

ENTRYPOINT ["ruby", "bin/wayback_machine_downloader"]

CMD []