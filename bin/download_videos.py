import base64
import hashlib
import json
import time
import yt_dlp as youtube_dl
from urllib.parse import urlparse, parse_qs
import os
import argparse
import re
import logging

# how to install yt_dlp
# python3 -m pip install --force-reinstall https://github.com/yt-dlp/yt-dlp/archive/master.tar.gz

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

meta_videos_success = []
meta_videos_failure = []

exclude_dir = "replaced_videos_"
data_dir = f"replaced_videos_{str(time.time())}/"


def export_log(directory: str):
    with open(f'{directory}success.json', 'w') as file:
        json.dump(meta_videos_success, file)
    with open(f'{directory}failure.json', 'w') as file:
        json.dump(meta_videos_failure, file)


def url_to_filename(url: str):
    allowed_characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
    # Using list comprehension to filter and retain only the allowed characters
    return ''.join(char for char in url if char in allowed_characters)


def save_video(url: str, output_directory: str):
    filename = url_to_filename(url)

    ydl_opts = {
        'format': 'bestvideo+bestaudio/best',
        'outtmpl': os.path.join(output_directory, filename + '.%(ext)s'),
    }

    try:
        with youtube_dl.YoutubeDL(ydl_opts) as ydl:
            info_dict = ydl.extract_info(url, download=False)
            video_title = info_dict.get('title', None)
            video_ext = info_dict.get('ext', None)

            ydl.download([url.strip()])
            meta_videos_success.append({
                "url" : url,
                "filename" : filename,
                "title" : video_title,
                "extension" : video_ext
            })

            logging.info(f"downloaded: {url}")
            return f"{filename}.{video_ext}"
        
    except Exception as e:
        if url not in meta_videos_failure:
            meta_videos_failure.append(url)
        logging.error(f"Error downloading {url}: {e}")
        return None


def replace_content(directory: str, url_mapping):
    for root, dirs, files in os.walk(directory):
        if exclude_dir in root:
            continue
        for file in files:
            try:
                file_path = os.path.join(root, file)
                with open(file_path, 'r', encoding='utf-8') as f:
                    content = f.read()

                for key, value in url_mapping.items():
                    content = content.replace(key, value)

                with open(file_path, 'w', encoding='utf-8') as f:
                    f.write(content)
            except Exception as e:
                logging.error(f"replace content: {e}")


def download_videos(directory: str, webroot: str):
    # Directory for storing videos
    video_dir = os.path.join(os.path.dirname(directory), data_dir)

    # Create the directory if it does not exist
    if not os.path.exists(video_dir):
        os.makedirs(video_dir)

    # Pattern for YouTube and Vimeo URLs (both http and https)
    url_pattern = (
        r'(https?://www\.youtube\.com/[^,<\\"\\>]*|'
        r'https?://youtu\.be/[^,<\\"\\>]*|'
        r'https?://vimeo\.com/[^,<\\"\\>]*|'
        r'https?://player\.vimeo\.com/[^,<\\"\\>]*)'
    )

    video_urls = []
    url_mapping = {}

    # Walk through directory and its subdirectories
    for root, dirs, files in os.walk(directory):
        if exclude_dir in root:
            continue
        for filename in files:
            file_path = os.path.join(root, filename)

            # Read the file and search for video URLs
            with open(file_path, 'r', encoding='utf-8') as file:
                try:
                    for line in file:
                        urls = re.findall(url_pattern, line)
                        for url in urls:
                            print(">>> " + url)
                            try:
                                if url not in video_urls:
                                    filename_and_ext = save_video(url, video_dir)
                                    if filename_and_ext != None:
                                        url_mapping[url] = f'{webroot}{data_dir}{filename_and_ext}'
                                    video_urls.append(url)
                            except Exception as e:
                                if url not in meta_videos_failure:
                                    meta_videos_failure.append(url)
                except Exception as e:
                    logging.error(f"error parsing html content: {e}")

    return url_mapping


def main():
    parser = argparse.ArgumentParser(description="download and replace videos of html pages")
    parser.add_argument('-dir', '--directory', required=True, help="path to the document root of webpage")
    parser.add_argument('-web', '--webroot', required=True, help="url to webroot of webpage")
    args = parser.parse_args()

    # download videos
    url_mapping = download_videos(args.directory, args.webroot)
    print(url_mapping)
    # replace urls
    replace_content(args.directory, url_mapping)
    # export log
    export_log(f'{args.directory}{data_dir}')
    logging.info("video replacement done")

if __name__ == "__main__":
    main()