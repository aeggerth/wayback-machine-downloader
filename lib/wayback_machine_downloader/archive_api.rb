require 'open-uri'
require 'net/http'

module ArchiveAPI

  # def get_raw_list_from_api url, page_index
  #   sleep(rand(4..8))

  #   # Define request url
  #   # request_url = "http://web.archive.org/cdx/search?url=#{url}&matchType=prefix&collapse=urlkey&output=json&fl=original,mimetype,timestamp,endtimestamp,groupcount,uniqcount&filter=!statuscode:[45]..&limit=100000&_=1529584262506"
  #   request_url = "http://web.archive.org/cdx/search/xd?url="
  #   request_url += url
  #   request_url += parameters_for_api page_index
  #   uri = URI(request_url)

  #   # Create the HTTP request
  #   http = Net::HTTP.new(uri.host, uri.port)
  #   request = Net::HTTP::Get.new(uri.request_uri)

  #   # Add headers to the request
  #   WaybackMachineDownloader.headers.each do |key, value|
  #     request[key] = value
  #   end

  #   # Make the request
  #   response = http.request(request).body

  #   return response
  # end

  def get_raw_list_from_api url, page_index
    begin
      sleep(rand(4..8))

      # Define request url
      # request_url = "http://web.archive.org/cdx/search?url=#{url}&matchType=prefix&collapse=urlkey&output=json&fl=original,mimetype,timestamp,endtimestamp,groupcount,uniqcount&filter=!statuscode:[45]..&limit=100000&_=1529584262506"
      request_url = "http://web.archive.org/cdx/search/xd?url="
      request_url += url
      request_url += parameters_for_api page_index
      uri = URI(request_url)

      # Create the HTTP request
      http = Net::HTTP.new(uri.host, uri.port)
      request = Net::HTTP::Get.new(uri.request_uri)

      # Add headers to the request
      WaybackMachineDownloader.headers.each do |key, value|
        request[key] = value
      end

      # Make the request
      response = http.request(request).body

      return response
    rescue => e
      sleep_time = rand(14..18)
      puts "Retry indexing in  seconds."
      # " + sleep_time + "
      sleep(sleep_time)
    end
  end

  def parameters_for_api page_index
    parameters = "&fl=timestamp,original&collapse=digest&gzip=false"
    if @all
      parameters += ""
    else
      parameters += "&filter=statuscode:200"
    end
    if @from_timestamp and @from_timestamp != 0
      parameters += "&from=" + @from_timestamp.to_s
    end
    if @to_timestamp and @to_timestamp != 0
      parameters += "&to=" + @to_timestamp.to_s
    end
    if page_index
      parameters += "&page=#{page_index}"
    end
    parameters
  end

end
